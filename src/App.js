import React from "react";
// import Preloader from "./utils/Preloader";
import Navbar from "./components/Header/index";
import Hero from "./components/Hero/index";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider, ThemeContext } from "./context/ThemeContext";

import "./assets/styles/all.scss";
import "./utils/assets/style.scss";

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <div className="App Cg ">
                    {/* <Preloader /> */}
                    <ThemeProvider>
                        <ThemeContext.Consumer>
                            {context => (
                                <>
                                    <Navbar />
                                    <Hero />
                                </>
                            )}
                        </ThemeContext.Consumer>
                    </ThemeProvider>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
