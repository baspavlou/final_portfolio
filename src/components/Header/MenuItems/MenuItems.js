import React from "react";
import Menuitemsdesc from "./Menuitemsdesc";

class Menuitems extends React.Component {
    constructor() {
        super();
        this.state = {
            descOpacity: 0,
            borderLeft: "none"
        };
        this.handleEnter = this.handleEnter.bind(this);
        this.handleLeave = this.handleLeave.bind(this);
    }
    handleEnter = () => {
        this.setState({
            descOpacity: 1
        });
    };
    handleLeave = () => {
        this.setState({
            descOpacity: 0
        });
    };

    render() {
        return (
            <div className="liWrapper">
                <div
                    className="flexCenter"
                    onMouseEnter={this.handleEnter}
                    onMouseLeave={this.handleLeave}
                >
                    {this.props.children}
                </div>
                <Menuitemsdesc
                    desc={this.props.desc}
                    descOpacity={this.state.descOpacity}
                    display={this.state.display}
                />
            </div>
        );
    }
}

export default Menuitems;
