import React from "react";

const Menuitemsdesc = props => {
    return (
        <div className="menu-desc-box">
            <p
                className="pdesc pdeschover"
                style={{
                    opacity: props.descOpacity,
                    transition: "opacity 0.3s ease"
                }}
            >
                {props.desc}
            </p>
        </div>
    );
};
export default Menuitemsdesc;
