import React, { Component } from "react";
import Menuitems from "./MenuItems";
import { Link } from "react-router-dom";
import ToggleTheme from "./ToggleTheme";

import "./assets/style.scss";

class Header extends Component {
    constructor() {
        super();
        this.state = {
            showToggle: true,
            color: "black"
        };
    }
    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }
    updateDimensions = () => {
        if (window.innerWidth > 525) {
            this.setState({
                showToggle: true
            });
        } else {
            this.setState({
                showToggle: false
            });
        }
    };

    render() {
        // console.log(window.innerWidth);
        return (
            <div className="navbar">
                <Menuitems id="home" desc="Home">
                    <Link to="/">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 30 30"
                        >
                            <g id="homeSVG" data-name="Layer 2">
                                <g id="_30249A.tif" data-name="30249A.tif">
                                    <path
                                        className="cls-1"
                                        d="M7.78,28.15H6.9a2.62,2.62,0,0,1-2.55-1.89c-.12-.4-.23-.8-.34-1.19-.06-.21-.11-.42-.17-.63l-.6-2.1c-.52-1.84-1.06-3.74-1.6-5.61a2.72,2.72,0,0,1,.85-3c1.09-1.07,2.18-2.18,3.23-3.25L7.08,9.12C8,8.19,8.92,7.25,9.85,6.32l3.75-3.8a2.41,2.41,0,0,1,1.7-.67h0a2.42,2.42,0,0,1,1.74.7l2.29,2.32c2.8,2.83,5.69,5.77,8.55,8.64l.17.18a2.71,2.71,0,0,1,1,1.59A3.27,3.27,0,0,1,29,16.59c-.91,3.27-1.83,6.5-2.7,9.52a2.6,2.6,0,0,1-2.7,2H21.55a2.61,2.61,0,0,1-2.75-2.77v-4c0-.74,0-1.49,0-2.24A2.1,2.1,0,0,0,16.65,17c-1,0-1.87,0-2.71,0a2.08,2.08,0,0,0-2.12,2.13c0,1.54,0,3.11,0,4.64v1.54A2.63,2.63,0,0,1,9,28.14H7.78ZM15.3,2.22h0a2,2,0,0,0-1.44.57L10.11,6.58,7.34,9.39,6,10.76C4.94,11.84,3.85,13,2.75,14A2.42,2.42,0,0,0,2,16.62c.54,1.87,1.09,3.77,1.61,5.61l.6,2.1c.06.21.11.42.17.63.11.39.22.79.34,1.17a2.26,2.26,0,0,0,2.2,1.64c.58,0,1.16,0,1.73,0H9a2.27,2.27,0,0,0,2.49-2.5V19.08a2.47,2.47,0,0,1,2.49-2.49c.85,0,1.74,0,2.73,0a2.46,2.46,0,0,1,2.51,2.51c0,.75,0,1.5,0,2.24v1.94c0,.7,0,1.39,0,2.09a2.24,2.24,0,0,0,2.37,2.4c.68,0,1.37,0,2,0A2.24,2.24,0,0,0,25.94,26c.87-3,1.8-6.25,2.7-9.52h0a2.84,2.84,0,0,0,0-1.15A2.4,2.4,0,0,0,27.79,14l-.18-.17L19.06,5.13,16.78,2.81A2.1,2.1,0,0,0,15.3,2.22Z"
                                    />
                                </g>
                                <path
                                    className="cls-1"
                                    d="M15.48,13.29a2.15,2.15,0,1,1,2.15-2.15A2.14,2.14,0,0,1,15.48,13.29Zm0-3.92a1.78,1.78,0,1,0,1.78,1.77A1.77,1.77,0,0,0,15.48,9.37Z"
                                />
                            </g>
                        </svg>
                    </Link>
                </Menuitems>

                <Menuitems id="about" desc="About Me">
                    <Link to="/about">
                        <svg
                            id="aboutSVG"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 30 30"
                        >
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="MbugnB.tif">
                                    <path
                                        className="cls-1"
                                        d="M15,27.92l-1.69,0c-1.33,0-2.71,0-4.06-.06a11.59,11.59,0,0,1-3.12-.41A3,3,0,0,1,4.9,26.8a1.72,1.72,0,0,1-.58-1.26,10.18,10.18,0,0,1,2.4-6.94,8.43,8.43,0,0,1,3.69-2.66,5.86,5.86,0,0,1,2-.37h5a6.85,6.85,0,0,1,4,1.32,10.06,10.06,0,0,1,3.5,4.41,10,10,0,0,1,.78,4.14,1.82,1.82,0,0,1-1,1.61,5,5,0,0,1-1.9.61,30.08,30.08,0,0,1-4.33.23H15.11Zm.55-12.06h-3.1a5.81,5.81,0,0,0-1.9.35A8.15,8.15,0,0,0,7,18.79a9.89,9.89,0,0,0-2.33,6.74,1.4,1.4,0,0,0,.48,1,2.63,2.63,0,0,0,1.08.56,11.75,11.75,0,0,0,3,.41c1.35,0,2.73,0,4.06.05l1.54,0h3.68a28.11,28.11,0,0,0,4.29-.23,4.88,4.88,0,0,0,1.79-.56,1.55,1.55,0,0,0,.82-1.37,9.52,9.52,0,0,0-.76-4,9.82,9.82,0,0,0-3.39-4.29,6.67,6.67,0,0,0-3.81-1.27Z"
                                    />
                                    <path
                                        className="cls-1"
                                        d="M15,15.11a6.7,6.7,0,0,1-3.89-1.32A6,6,0,0,1,8.5,9.59,6.1,6.1,0,0,1,10.38,4a6.35,6.35,0,0,1,6.86-1.48,6.26,6.26,0,0,1,4.27,5.14,1.65,1.65,0,0,1,0,.32l0,.44V8.6h0a6.29,6.29,0,0,1-3.5,5.7A6.76,6.76,0,0,1,15,15.11ZM14.87,2.38a6.11,6.11,0,0,0-4.28,1.83,5.8,5.8,0,0,0-1.8,5.33,5.78,5.78,0,0,0,2.45,4A6.07,6.07,0,0,0,18,14,6,6,0,0,0,21.29,8.6V8.43l0-.44a1.27,1.27,0,0,0,0-.28,6,6,0,0,0-4.08-4.92A6.9,6.9,0,0,0,14.87,2.38Z"
                                    />
                                </g>
                            </g>
                        </svg>
                    </Link>
                </Menuitems>

                <Menuitems id="resume" desc="Resume">
                    <Link to="/resume">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            id="resumeSVG"
                            viewBox="0 0 30 30"
                        >
                            <g id="Layer_2" data-name="Layer 2">
                                <path
                                    className="cls-1"
                                    d="M24.52,28.28H5.66A3.29,3.29,0,0,1,2.37,25V6.13A3.29,3.29,0,0,1,5.66,2.84H24.52a3.3,3.3,0,0,1,3.29,3.29V25A3.29,3.29,0,0,1,24.52,28.28ZM5.66,3.19A3,3,0,0,0,2.71,6.13V25a3,3,0,0,0,3,3H24.52a3,3,0,0,0,2.94-3V6.13a2.94,2.94,0,0,0-2.94-2.94Z"
                                />
                                <path
                                    className="cls-1"
                                    d="M9.21,10.68H6.68a1,1,0,0,1-1-1V7.15a1,1,0,0,1,1-1H9.21a1,1,0,0,1,1,1V9.68A1,1,0,0,1,9.21,10.68ZM6.68,6.5A.66.66,0,0,0,6,7.15V9.68a.67.67,0,0,0,.66.66H9.21a.66.66,0,0,0,.65-.66V7.15a.65.65,0,0,0-.65-.65Z"
                                />
                                <rect
                                    className="cls-1"
                                    x="5.85"
                                    y="14.52"
                                    width="18.47"
                                    height="0.35"
                                />
                                <rect
                                    className="cls-1"
                                    x="5.94"
                                    y="17.48"
                                    width="18.47"
                                    height="0.35"
                                />
                                <rect
                                    className="cls-1"
                                    x="5.85"
                                    y="19.92"
                                    width="18.47"
                                    height="0.35"
                                />
                                <rect
                                    className="cls-1"
                                    x="5.94"
                                    y="22.88"
                                    width="18.47"
                                    height="0.35"
                                />
                            </g>
                        </svg>
                    </Link>
                </Menuitems>
                <Menuitems id="portfolio" desc="Portfolio">
                    <Link to="/portfolio">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 30 30"
                        >
                            <g id="Layer_2" data-name="Layer 2">
                                <path
                                    className="cls-1"
                                    d="M21.33,23.88l-.26-.27L27.68,17a2.09,2.09,0,0,0,0-2.95L21.07,7.44l.26-.26,6.61,6.6a2.47,2.47,0,0,1,0,3.49Z"
                                />
                                <path
                                    className="cls-1"
                                    d="M8.67,23.88,2.06,17.27a2.47,2.47,0,0,1,0-3.49l6.61-6.6.26.26L2.32,14.05a2.09,2.09,0,0,0,0,2.95l6.61,6.61Z"
                                />
                                <path
                                    className="cls-1"
                                    d="M12.78,25.53l-.3-.22.15.11-.15-.11c.33-.54,2.75-12.09,4.29-19.72l.36.08C16.73,7.63,13.23,24.92,12.78,25.53Z"
                                />
                            </g>
                        </svg>
                    </Link>
                </Menuitems>
                <Menuitems id="contact" desc="Contact">
                    <Link to="/contact">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 30 30"
                        >
                            <g id="Layer_2" data-name="Layer 2">
                                <path
                                    className="cls-1"
                                    d="M18.45,10.53a3.07,3.07,0,0,1,0-6.14h0a3.07,3.07,0,0,1,0,6.14Zm0-5.78h0a2.71,2.71,0,1,0,0,5.42h0a2.71,2.71,0,0,0,0-5.42Z"
                                />
                                <rect
                                    className="cls-1"
                                    x="7.91"
                                    y="11.81"
                                    width="9.18"
                                    height="0.36"
                                    transform="matrix(0.8, -0.6, 0.6, 0.8, -4.7, 9.92)"
                                />
                                <path
                                    className="cls-1"
                                    d="M24.13,25.61a3.47,3.47,0,1,1,3.46-3.46A3.47,3.47,0,0,1,24.13,25.61Zm0-6.57A3.13,3.13,0,0,0,21,22.15,3.11,3.11,0,1,0,24.13,19Z"
                                />
                                <path
                                    className="cls-1"
                                    d="M5.87,19.86a3.47,3.47,0,0,1-3.46-3.47,3.46,3.46,0,0,1,3.46-3.46,3.47,3.47,0,1,1,0,6.93Zm0-6.57a3.09,3.09,0,0,0-3,2.19,3.11,3.11,0,1,0,3-2.19Z"
                                />
                                <rect
                                    className="cls-1"
                                    x="14.73"
                                    y="13.3"
                                    width="0.36"
                                    height="12.57"
                                    transform="matrix(0.25, -0.97, 0.97, 0.25, -7.83, 29.03)"
                                />
                            </g>
                        </svg>
                    </Link>
                </Menuitems>
                {this.state.showToggle && <ToggleTheme />}
            </div>
        );
    }
}
export default Header;
