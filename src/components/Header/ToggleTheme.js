import React, { Component } from "react";
import ThemeContext from "../../context/ThemeContext";

class ToggleTheme extends Component {
    constructor() {
        super();
        this.state = {};
    }
    render() {
        return (
            <ThemeContext.Consumer>
                {context => {
                    return (
                        <svg
                            id="toggleThemeIcon"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 100 100"
                            onClick={() => context.toggle()}
                        >
                            <g id="Layer_2" data-name="Layer 2">
                                <path d="M97.37,34A50.29,50.29,0,0,0,67.19,3s0,0,0,0L66,2.63a50,50,0,1,0-4.73,96.09c.8-.19,1.6-.39,2.39-.62s1.56-.46,2.34-.73c1.16-.39,2.31-.83,3.43-1.3h0c1.12-.48,2.21-1,3.29-1.54q.54-.27,1.08-.57A50.48,50.48,0,0,0,95.58,70.58h0c.5-1.11,1-2.23,1.38-3.38A50.08,50.08,0,0,0,97.37,34ZM2.89,50A47.17,47.17,0,0,1,50,2.89V97.12A47.17,47.17,0,0,1,2.89,50Z" />
                                <path d="M66,97.37c1.16-.39,2.31-.83,3.43-1.3C68.33,96.55,67.18,97,66,97.37Z" />
                                <path d="M69.46,96.07c1.12-.48,2.21-1,3.29-1.54C71.68,95.08,70.58,95.6,69.46,96.07Z" />
                                <path d="M95.58,70.58A50.09,50.09,0,0,1,73.83,94,50.48,50.48,0,0,0,95.58,70.58Z" />
                                <path d="M67.19,3A50.09,50.09,0,0,1,97.37,34,50.29,50.29,0,0,0,67.19,3Z" />
                                <path d="M95.58,70.57c.5-1.11,1-2.23,1.38-3.38C96.54,68.34,96.08,69.47,95.58,70.57Z" />
                            </g>
                        </svg>
                    );
                }}
            </ThemeContext.Consumer>
        );
    }
}
export default ToggleTheme;
