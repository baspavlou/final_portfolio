import React from "react";
import { Switch, Route } from "react-router-dom";
import { About } from "../MainSections/About/index.js";
import { Portfolio } from "../MainSections/Portfolio/index.js";
import { Resume } from "../MainSections/Resume/index.js";
import { Contact } from "../MainSections/Contact/index.js";
import Home from "../Home/index";
import "./assets/style.scss";

class Hero extends React.Component {
    render() {
        return (
            <div
                className="hero-animated flexCenter"
                style={{
                    height: `${window.innerHeight}px`
                }}
            >
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/about" component={About} />
                    <Route exact path="/resume" component={Resume} />
                    <Route exact path="/portfolio" component={Portfolio} />
                    <Route exact path="/contact" component={Contact} />
                </Switch>
            </div>
        );
    }
}
export default Hero;
