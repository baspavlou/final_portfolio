import React, { Component } from "react";
import "./assets/style.scss";
class Home extends Component {
    constructor() {
        super();
        this.state = {
            txtElement: document.querySelector(".txt-type"),
            words: ["Full Stack Developer", "Software Engineer"],
            txt: "",
            wordIndex: 0,
            isDeleting: false,
            counter: 0,
            pause: false
        };
    }
    componentDidMount() {
        this.typeWriterInterval = setInterval(() => {
            if (!this.state.pause) {
                this.type();
            } else {
                this.setState({
                    counter: this.state.counter + 1
                });
            }
            if (this.state.counter > 8) {
                this.setState({
                    pause: false,
                    counter: 0
                });
            }
        }, 100);
    }
    componentWillUnmount() {
        clearInterval(this.typeWriterInterval);
    }

    type = () => {
        // Current index of word
        const current = this.state.wordIndex % this.state.words.length;
        // Get full text of current word
        const fullTxt = this.state.words[current];

        // Check if deleting
        if (this.state.isDeleting) {
            // Remove char
            this.setState({
                txt: fullTxt.substring(0, this.state.txt.length - 1)
            });
        } else {
            // Add char
            this.setState({
                txt: fullTxt.substring(0, this.state.txt.length + 1)
            });
        }
        // // Insert txt into element
        document.querySelector(
            ".txt-type .txt"
        ).innerHTML = `${this.state.txt}`;

        if (!this.state.isDeleting && this.state.txt === fullTxt) {
            // Set delete to true
            this.setState({
                pause: true,
                isDeleting: true
            });
        } else if (this.state.isDeleting && this.state.txt === "") {
            this.setState({
                isDeleting: false
            });
            // Move to next word
            this.setState({
                wordIndex: this.state.wordIndex + 1
            });
        }
    };
    render() {
        return (
            <div className="flexCenter typewriter-container">
                <div>
                    <h2>Basilis Pavlou</h2>
                    <h2 className="typewriter">
                        <span className="txt-type">
                            <span className="txt"></span>
                        </span>
                    </h2>
                </div>
            </div>
        );
    }
}
export default Home;
