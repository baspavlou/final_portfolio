import React, { Component } from "react";
import SectionHeader from "../../../utils/SectionHeader";
import Fade from "react-reveal/Fade";
import Headings from "../../../utils/Headings";
import LanguagesLogos from "./LanguagesLogo";
import "./assets/style.scss";

class Resume extends Component {
    render() {
        return (
            <Fade duration={400}>
                <div className="flexCenter section-container resume">
                    <Fade duration={600} delay={500}>
                        <SectionHeader text="Resume">
                            <span className="section-header-span">3</span> years
                            of experience
                        </SectionHeader>
                    </Fade>
                    <Fade duration={600} delay={700}>
                        <div
                            style={{
                                width: "100%"
                            }}
                        >
                            <Headings text="Skills" />
                            <LanguagesLogos />
                        </div>
                    </Fade>
                </div>
            </Fade>
        );
    }
}
export default Resume;
