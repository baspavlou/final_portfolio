import React, { Component } from "react";
import Fade from "react-reveal/Fade";
import SectionHeader from "../../../utils/SectionHeader";
import Headings from "../../../utils/Headings";
import ServicesComponent from "./ServicesComponent";
// import servicesData from "./servicesData";
import Button from "../../../utils/Button";
import nextIcon from "../../../assets/nextIcon.svg";
import "./assets/style.scss";

class About extends Component {
    constructor() {
        super();
        this.state = {
            // services: servicesData
        };
    }
    render() {
        return (
            <Fade duration={400}>
                <div className=" section-container about ">
                    <SectionHeader text="About Me">
                        <span className="section-header-span">
                            Hello, I'm Basilis Pavlou{" "}
                        </span>
                        and I am a Web Developer currently living in Athens,
                        Greece .
                    </SectionHeader>
                    <Headings text="Services" />
                    <div className="services-box-container">
                        <ServicesComponent />
                    </div>

                    <Button
                        width={160}
                        mt={30}
                        logo={nextIcon}
                        text="Download CV"
                        target="_blank"
                        link="https://bpavlou.gr/BasilisPavlou.pdf"
                    />
                </div>
            </Fade>
        );
    }
}
export default About;
