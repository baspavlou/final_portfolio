import React, { Component } from "react";

class PortfolioGrid extends Component {
    render() {
        let { bg, img, link, techs, desc, color } = this.props;
        return (
            <a
                className="portfolio-tile  flexCenter"
                href={link}
                target="_blank"
                rel="noopener noreferrer"
                style={{
                    backgroundColor: bg
                }}
            >
                <div className="project-info flexCenter">
                    <img src={img} alt="Avatar" className="project-image" />
                    <p style={{ color: color }}>{desc} </p>
                </div>

                <div className="overlay">
                    <p>{techs} </p>
                </div>
            </a>
        );
    }
}
export default PortfolioGrid;
