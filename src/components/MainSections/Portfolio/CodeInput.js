import React, { Component } from "react";
import $ from "jquery";

class CodeInput extends Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = e => {
        let data = {
            val: e.target.value
        };
        let self = this;
        $.ajax({
            url: `https://bpavlou.gr/response.php`,
            method: "post",
            data: data,
            success: function(response) {
                let obj = JSON.parse(response);
                if (obj.message === "success") {
                    self.props.action();
                }
            }
        });
    };

    render() {
        return (
            <div className="portfolio-code-wrapper flexCenter">
                <p>
                    Please input the code you have received to see my projects
                </p>
                <form>
                    <input
                        onChange={this.handleChange}
                        autoFocus
                        type="password"
                    />
                </form>
            </div>
        );
    }
}
export default CodeInput;
