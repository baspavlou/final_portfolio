import React, { Component } from "react";
import Fade from "react-reveal/Fade";
import SectionHeader from "../../../utils/SectionHeader";
import Headings from "../../../utils/Headings";
import PortfolioGrid from "./PortfolioGrid";
import geusi from "../../../assets/projects/geusi.png";
import olives from "../../../assets/projects/olives.svg";
import go from "../../../assets/projects/go.png";
import cube from "../../../assets/projects/cube.svg";
import lasertag from "../../../assets/projects/lasertag.png";
// import ffc from "../../../assets/projects/ffc.jpg";
import "./assets/style.scss";
import CodeInput from "./CodeInput";

class Portfolio extends Component {
    constructor() {
        super();
        this.state = {
            showGrid: false
        };
        this.showGrid = this.showGrid.bind(this);
    }
    showGrid = () => {
        this.setState({
            showGrid: true
        });
    };

    render() {
        return (
            <Fade duration={400}>
                <div className="flexCenter section-container portfolio">
                    <Fade duration={600} delay={500}>
                        <SectionHeader text="Portfolio">
                            <span className="section-header-span">
                                Please take a look
                            </span>{" "}
                            at some of the projects i have been involved
                        </SectionHeader>
                    </Fade>
                    <Fade duration={600} delay={700}>
                        <div
                            style={{
                                width: "100%"
                            }}
                        >
                            <Headings text="Projects" />
                            {!this.state.showGrid ? (
                                <CodeInput action={this.showGrid} />
                            ) : (
                                <div className="portfolio-grid-container">
                                    <PortfolioGrid
                                        bg="white"
                                        color="black"
                                        techs={["HTML5,CSS3,JS"]}
                                        link={"https://www.escapegameover.com/"}
                                        desc={"Game Over HQ"}
                                        img={go}
                                    />

                                    <PortfolioGrid
                                        bg="black"
                                        color="white"
                                        techs={["HTML5,CSS3,JS"]}
                                        link={"#"}
                                        desc={"Cube Challenges"}
                                        img={cube}
                                    />
                                    <PortfolioGrid
                                        bg="white"
                                        techs={["HTML5,CSS3,JS,Bootstrap"]}
                                        link={"https://www.geusipatra.gr/"}
                                        color="black"
                                        desc={"Geusi Patra"}
                                        img={geusi}
                                    />
                                    <PortfolioGrid
                                        bg="white"
                                        color="black"
                                        techs={["HTML5,CSS3,JS,Bootstrap"]}
                                        link={"https://www.escapegameover.fr/"}
                                        desc={"Game Over FR"}
                                        img={go}
                                    />
                                    <PortfolioGrid
                                        bg="black"
                                        color="white"
                                        techs={["HTML5,CSS3,JS,Bootstrap"]}
                                        link={"https://www.lasertagarena.gr/"}
                                        desc={"LaserTag Arena"}
                                        img={lasertag}
                                    />
                                    <PortfolioGrid
                                        bg="#232121"
                                        techs={["HTML5,CSS3,JS"]}
                                        link={"https://www.akritochori.gr/"}
                                        desc={"Akritochori Messinias"}
                                        color="white"
                                        img={olives}
                                    />
                                    {/* <PortfolioGrid
                                        bg="#006401"
                                        color="white"
                                        techs={["HTML5,CSS3,JS"]}
                                        link={
                                            "https://baspavlou.github.io/Random-Quote-Generator/"
                                        }
                                        desc={"Quote Generator"}
                                        img={ffc}
                                    /> */}
                                </div>
                            )}
                        </div>
                    </Fade>
                </div>
            </Fade>
        );
    }
}
export default Portfolio;
