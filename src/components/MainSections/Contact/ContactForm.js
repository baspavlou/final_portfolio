import React, { Component } from "react";
import Button from "../../../utils/Button";
import logo from "../../../assets/nextIcon.svg";

import $ from "jquery";

class ContactForm extends Component {
    constructor() {
        super();
        this.state = {
            name: "",
            email: "",
            message: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({
            [name]: value
        });
    }

    submitForm = () => {
        let { name, email, message } = this.state;
        if (name !== "" && email !== "" && message !== "") {
            // let self = this;

            let data = {
                name,
                email,
                message
            };
            console.log("i will send data");
            $.ajax({
                url: `https://bpavlou.gr/base.php`,
                method: "post",
                data: data,
                success: function(response) {}
            });
        }
    };
    render() {
        return (
            <form id="contactForm">
                <input
                    type="text"
                    name="name"
                    required
                    maxLength="25"
                    value={this.state.name}
                    onChange={this.handleChange}
                    autoComplete="dewdddd"
                    placeholder="Enter your Name"
                />

                <input
                    type="email"
                    maxLength="25"
                    name="email"
                    required
                    value={this.state.email}
                    onChange={this.handleChange}
                    autoComplete="dewdewwwwww"
                    placeholder="Enter your email"
                />
                <textarea
                    name="message"
                    maxLength="300"
                    rows="6"
                    required
                    value={this.state.message}
                    onChange={this.handleChange}
                    autoComplete="ewewdewdew"
                    placeholder="Enter your message"
                />

                <Button
                    width={110}
                    logo={logo}
                    text="Submit"
                    link=""
                    action={this.submitForm}
                />
            </form>
        );
    }
}
export default ContactForm;
