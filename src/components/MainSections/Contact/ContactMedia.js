import React, { Component } from "react";
import phone from "./assets/contact_icons/phone.svg";
import envelope from "./assets/contact_icons/envelope.svg";

class ContactMedia extends Component {
    render() {
        return (
            <div className="contact-media-container">
                <div className="contact-box first">
                    <img src={phone} alt="phone" />
                    <p>+30 697 953 1232</p>
                </div>
                <div className="contact-box">
                    <img src={envelope} alt="email" />
                    <p>
                        <a href="mailto:info@bpavlou.gr">info@bpavlou.gr</a>
                    </p>
                </div>
                <div className="social-media">
                    {/* <img src={instagram} alt="instagram" /> */}
                    <a
                        href="https://github.com/baspavlou"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <svg
                            height="512px"
                            id="Layer_1"
                            version="1.1"
                            viewBox="0 0 512 512"
                            xmlSpace="preserve"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                        >
                            <g>
                                <path
                                    class="st0"
                                    d="M256,32C132.3,32,32,134.8,32,261.7c0,101.5,64.2,187.5,153.2,217.9c11.2,2.1,15.3-5,15.3-11.1   c0-5.5-0.2-19.9-0.3-39.1c-62.3,13.9-75.5-30.8-75.5-30.8c-10.2-26.5-24.9-33.6-24.9-33.6c-20.3-14.3,1.5-14,1.5-14   c22.5,1.6,34.3,23.7,34.3,23.7c20,35.1,52.4,25,65.2,19.1c2-14.8,7.8-25,14.2-30.7c-49.7-5.8-102-25.5-102-113.5   c0-25.1,8.7-45.6,23-61.6c-2.3-5.8-10-29.2,2.2-60.8c0,0,18.8-6.2,61.6,23.5c17.9-5.1,37-7.6,56.1-7.7c19,0.1,38.2,2.6,56.1,7.7   c42.8-29.7,61.5-23.5,61.5-23.5c12.2,31.6,4.5,55,2.2,60.8c14.3,16.1,23,36.6,23,61.6c0,88.2-52.4,107.6-102.3,113.3   c8,7.1,15.2,21.1,15.2,42.5c0,30.7-0.3,55.5-0.3,63c0,6.1,4,13.3,15.4,11C415.9,449.1,480,363.1,480,261.7   C480,134.8,379.7,32,256,32z"
                                />
                            </g>
                        </svg>
                    </a>
                    <a
                        href="https://www.facebook.com/basilispavlou.official"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <svg
                            height="67px"
                            id="Layer_1"
                            version="1.1"
                            viewBox="0 0 67 67"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                        >
                            <path d="M28.765,50.32h6.744V33.998h4.499l0.596-5.624h-5.095  l0.007-2.816c0-1.466,0.14-2.253,2.244-2.253h2.812V17.68h-4.5c-5.405,0-7.307,2.729-7.307,7.317v3.377h-3.369v5.625h3.369V50.32z   M33,64C16.432,64,3,50.568,3,34C3,17.431,16.432,4,33,4s30,13.431,30,30C63,50.568,49.568,64,33,64z" />
                        </svg>
                    </a>
                    <a
                        href="https://www.linkedin.com/in/basilis-pavlou/"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <svg
                            height="67px"
                            id="Layer_1"
                            version="1.1"
                            viewBox="0 0 67 67"
                            xmlSpace="preserve"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                        >
                            <path d="M50.837,48.137V36.425c0-6.275-3.35-9.195-7.816-9.195  c-3.604,0-5.219,1.983-6.119,3.374V27.71h-6.79c0.09,1.917,0,20.427,0,20.427h6.79V36.729c0-0.609,0.044-1.219,0.224-1.655  c0.49-1.22,1.607-2.483,3.482-2.483c2.458,0,3.44,1.873,3.44,4.618v10.929H50.837z M22.959,24.922c2.367,0,3.842-1.57,3.842-3.531  c-0.044-2.003-1.475-3.528-3.797-3.528s-3.841,1.524-3.841,3.528c0,1.961,1.474,3.531,3.753,3.531H22.959z M34,64  C17.432,64,4,50.568,4,34C4,17.431,17.432,4,34,4s30,13.431,30,30C64,50.568,50.568,64,34,64z M26.354,48.137V27.71h-6.789v20.427  H26.354z" />
                        </svg>
                    </a>
                    <a
                        href="https://api.whatsapp.com/send?phone=306979531232"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <svg
                            height="56.6934px"
                            id="Layer_1"
                            version="1.1"
                            viewBox="0 0 56.6934 56.6934"
                            width="56.6934px"
                            xmlSpace="preserve"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                        >
                            <g>
                                <path d="M29.7518,16.1734c-6.7766,0-12.2879,5.5108-12.2905,12.2844c-0.001,2.3214,0.6486,4.5821,1.8783,6.5381l0.2922,0.4648   l-1.2411,4.5334l4.6498-1.2197l0.4485,0.2662c1.8863,1.1193,4.0485,1.7116,6.2529,1.7125h0.005   c6.7715,0,12.2826-5.5113,12.2853-12.2855c0.0013-3.2826-1.2754-6.3693-3.5948-8.6915   C36.1179,17.4541,33.0332,16.1744,29.7518,16.1734z M36.9781,33.7399c-0.3077,0.8626-1.7833,1.65-2.4929,1.756   c-0.6364,0.0952-1.4415,0.1348-2.3262-0.1463c-0.5364-0.1702-1.2243-0.3975-2.1055-0.778   c-3.7049-1.5998-6.1246-5.3301-6.3092-5.5767c-0.1846-0.2464-1.5081-2.0026-1.5081-3.8206c0-1.8177,0.9542-2.7113,1.2927-3.0811   c0.3384-0.3696,0.7386-0.4621,0.9848-0.4621c0.2462,0,0.4927,0.0023,0.7078,0.013c0.2267,0.0115,0.5312-0.0861,0.8311,0.634   c0.3077,0.7396,1.0463,2.5575,1.1387,2.7424c0.0924,0.1848,0.1539,0.4005,0.0308,0.647c-0.1231,0.2463-0.1846,0.4005-0.3693,0.6161   c-0.1846,0.2157-0.3879,0.4815-0.554,0.647c-0.1849,0.1842-0.3774,0.3841-0.162,0.7537c0.2155,0.3699,0.9567,1.5792,2.0547,2.5586   c1.4106,1.2582,2.6007,1.6482,2.97,1.8331c0.3693,0.185,0.5848,0.1539,0.8002-0.0924c0.2155-0.2465,0.9233-1.0785,1.1695-1.4482   c0.2462-0.3696,0.4924-0.308,0.8309-0.1848c0.3385,0.1234,2.1544,1.0167,2.5237,1.2015c0.3693,0.1849,0.6155,0.2774,0.7078,0.4315   C37.2859,32.1375,37.2859,32.877,36.9781,33.7399z" />
                                <path d="M29.2066,4.3736c-13.5996,0-24.625,11.0234-24.625,24.623s11.0254,24.625,24.625,24.625   c13.5986,0,24.624-11.0254,24.624-24.625S42.8052,4.3736,29.2066,4.3736z M29.747,43.2496   C29.7465,43.2496,29.7473,43.2496,29.747,43.2496h-0.0061c-2.4738-0.0009-4.9047-0.6216-7.0635-1.7991l-7.8357,2.0554l2.097-7.6594   c-1.2935-2.2416-1.9741-4.7843-1.973-7.3895c0.0032-8.1496,6.6341-14.78,14.7812-14.78c3.954,0.0016,7.6653,1.5409,10.4559,4.3348   c2.7907,2.7938,4.3268,6.5074,4.3253,10.457C44.5246,36.6185,37.8933,43.2496,29.747,43.2496z" />
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
        );
    }
}
export default ContactMedia;
