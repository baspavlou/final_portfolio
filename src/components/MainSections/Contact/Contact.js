import React, { Component } from "react";
import Fade from "react-reveal/Fade";
import SectionHeader from "../../../utils/SectionHeader";
import Headings from "../../../utils/Headings";
import ContactForm from "./ContactForm";
import ContactMedia from "./ContactMedia";
import "./assets/style.scss";
class Contact extends Component {
    render() {
        return (
            <Fade duration={400}>
                <div className="flexCenter section-container contact">
                    <Fade duration={600} delay={500}>
                        <SectionHeader text="Contact">
                            If you would like to get in touch,{" "}
                            <span className="section-header-span">
                                please shoot me an email
                            </span>{" "}
                            or fill the form below
                        </SectionHeader>
                    </Fade>

                    <div className="container two-col">
                        <div className="col">
                            <Fade duration={600} delay={700}>
                                <Headings text="Contact Form" />
                                <ContactForm />
                            </Fade>
                        </div>
                        <div className="col">
                            <Fade duration={600} delay={900}>
                                <Headings text="Get In Touch" />
                                <ContactMedia />
                            </Fade>
                        </div>
                    </div>
                </div>
            </Fade>
        );
    }
}
export default Contact;
