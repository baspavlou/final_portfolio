import React from "react";
import { lightTheme, darkTheme } from "./themes.js";
export const ThemeContext = React.createContext();

export class ThemeProvider extends React.Component {
    constructor() {
        super();
        this.state = {
            dark: false,
            toggle: () => {
                if (!this.state.dark) {
                    this.applyTheme(darkTheme);
                } else {
                    this.applyTheme(lightTheme);
                }

                this.setState({ dark: !this.state.dark });
            }
        };
    }
    applyTheme = theme => {
        const root = document.getElementsByTagName("html")[0];
        root.style.cssText = theme.join(";");
    };

    // toggle = () => {
    //     window.localStorage.setItem("darkTheme", !this.state.dark);
    //     this.setState({
    //         dark: !this.state.dark
    //     });
    // };
    render() {
        return (
            <ThemeContext.Provider
                value={{
                    dark: this.state.dark,
                    toggle: this.state.toggle
                }}
            >
                {this.props.children}
            </ThemeContext.Provider>
        );
    }
}
export default ThemeContext;
