export const lightTheme = [
    "--headings-bg: #f0f0f0",
    "--blue: #0080ff",
    "--pages-titles: #e5e5e5",
    "--gray-black: #202020",
    "--black: #000000",
    "--white: #f0f0f0",
    "--whiter: #ffffff"
];

export const darkTheme = [
    "  --headings-bg: #f0f0f0",
    "  --blue: #389d1c",
    "  --pages-titles: #a6a2a2",
    "  --gray-black: #e5e5e5",
    "  --black: #ffffff",
    "  --white: black",
    "  --whiter: #202020"
];
