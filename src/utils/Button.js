import React, { Component } from "react";

class Button extends Component {
    constructor() {
        super();
        this.state = {
            backgroundColor: "white",
            color: "rgb(20, 33, 61)"
        };
        this.handleEnter = this.handleEnter.bind(this);
        this.handleLeave = this.handleLeave.bind(this);
        this.handleAction = this.handleAction.bind(this);
    }
    componentDidMount = () => {
        this.setState({
            width: this.props.width
        });
    };
    handleEnter = () => {
        this.setState({
            backgroundColor: "rgb(20, 33, 61)",
            width: this.state.width + 35,
            color: "white"
        });
    };
    handleAction = () => {
        if (this.props.action) {
            this.props.action();
        }
    };
    handleLeave = () => {
        this.setState({
            backgroundColor: "white",
            width: this.state.width - 35,
            color: "rgb(20, 33, 61)"
        });
    };
    render() {
        return (
            <div
                style={{
                    display: "flex",
                    padding: "20px 0",
                    alignContent: "center"
                }}
            >
                {this.props.link !== "" ? (
                    <a
                        className="flexCenter button"
                        target={this.props.target}
                        href={this.props.link}
                        onMouseEnter={this.handleEnter}
                        onMouseLeave={this.handleLeave}
                        onClick={this.handleAction}
                        style={{
                            backgroundColor: this.state.backgroundColor,
                            color: this.state.color,
                            margin: `${this.props.mt}px auto 0 auto`,
                            width: `${this.state.width}px`
                        }}
                    >
                        {this.props.text}
                        <img
                            src={this.props.logo}
                            alt="Send"
                            style={{
                                boxSizing: "content-box"
                            }}
                        />
                    </a>
                ) : (
                    <button
                        className="flexCenter button"
                        onMouseEnter={this.handleEnter}
                        onMouseLeave={this.handleLeave}
                        onClick={this.handleAction}
                        style={{
                            backgroundColor: this.state.backgroundColor,
                            color: this.state.color,
                            margin: `${this.props.mt}px auto 0 auto`,
                            width: `${this.state.width}px`
                        }}
                    >
                        {this.props.text}
                        <img
                            src={this.props.logo}
                            alt="Send"
                            style={{
                                boxSizing: "content-box"
                            }}
                        />
                    </button>
                )}
            </div>
        );
    }
}
export default Button;
