import React from 'react'

const Headings = (props) => {
  return (
    <h2 className="h2-headings">
    {props.text}
    </h2>
  )
}

export default Headings
