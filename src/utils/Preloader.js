import React, { Component } from "react";

class Preloader extends Component {
    state = {
        opacity: "1",
        display: "flex"
    };

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                opacity: "0"
            });
        }, 2000);
        setTimeout(() => {
            this.setState({
                display: "none"
            });
        }, 3000);
    }

    render() {
        return (
            <div
                className="preloader"
                style={{
                    opacity: this.state.opacity,
                    transition: "all 1s ease",
                    display: this.state.display
                }}
            >
                <svg
                    version="1.1"
                    id="L9"
                    xmlns="http://www.w3.org/2000/svg"
                    x="0px"
                    y="0px"
                    viewBox="0 0 100 100"
                >
                    <rect x="37" y="40" width="4" height="10" fill="#fff">
                        <animateTransform
                            attributeType="xml"
                            attributeName="transform"
                            type="translate"
                            values="0 0; 0 20; 0 0"
                            begin="0"
                            dur="0.6s"
                            repeatCount="indefinite"
                        />
                    </rect>
                    <rect x="47" y="40" width="4" height="10" fill="#fff">
                        <animateTransform
                            attributeType="xml"
                            attributeName="transform"
                            type="translate"
                            values="0 0; 0 20; 0 0"
                            begin="0.2s"
                            dur="0.6s"
                            repeatCount="indefinite"
                        />
                    </rect>
                    <rect x="57" y="40" width="4" height="10" fill="#fff">
                        <animateTransform
                            attributeType="xml"
                            attributeName="transform"
                            type="translate"
                            values="0 0; 0 20; 0 0"
                            begin="0.4s"
                            dur="0.6s"
                            repeatCount="indefinite"
                        />
                    </rect>
                </svg>
            </div>
        );
    }
}

export default Preloader;
