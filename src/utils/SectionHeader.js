import React from "react";

const SectionHeader = props => {
  return (
    <div
      className="sections-headers">
      <h1>{props.text}</h1>
      <p>{props.children}</p>
    </div>
  );
};

export default SectionHeader;
